<?php

namespace Drupal\d8learningtask\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class GlobalColorForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'd8learningtask.globalcolors',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'global_colors_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('d8learningtask.globalcolors');
    $form['global_colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Color Values'),
      '#description' => $this->t('Please enter the values in key|value format. For example, red|Red. Please separated comma separated values for multiple values.'),
      '#default_value' => !empty($config->get('global_colors')) ? $config->get('global_colors') : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('d8learningtask.globalcolors')
      ->set('global_colors', $form_state->getValue('global_colors'))
      ->save();
    drupal_set_message($this->t('Color configurations have been saved.'));
  }

}
